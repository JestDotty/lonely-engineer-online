var CES = require('ces')

var ai = {

	Spawner: CES.Component.extend({
		name: 'spawner',
		imaginationRequired: 1,
		chance: 0.1,
		imaginationMultiplier: 1,
		init: function(imaginationRequired, chance, imaginationMultiplier){
			this.imaginationRequired = imaginationRequired
			this.chance = chance
			this.imaginationMultiplier = imaginationMultiplier
		}
	}),
	Seeker: CES.Component.extend({
		name: 'seeker',
		seeks: [],
		init: function(){
			seeks.push(arguments)
		}
	}),
	Scanner: CES.Component.extend({
		name: 'scanner',
		scansFor: [],
		init: function(){
			scansFor.push(arguments)
		}
	}),
	Mover: CES.Component.extend({
		name: 'mover',
		turns: 1,
		init: function(turn){
			this.turns = turns
		}
	})

}


module.exports = ai