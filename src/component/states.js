var CES = require('ces')

var states = {
	Solid: CES.Component.extend({
		name: 'solid',
		solid: 0,
		init: function(solid){
			this.solid = solid
		}
	}),

	Dead: CES.Component.extend({
		name: 'dead',
		init: function(killer, score, msg){
			this.killer = killer
			this.score = score
			this.msg = msg
		}
	})

}

module.exports = states