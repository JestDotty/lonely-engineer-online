var CES = require('ces')

var log = require('../log')

var gameData = require('../data')

module.exports = CES.System.extend({
	map: [],
	init: function(width, height){
		for(var x=0; x<width; x++){
			this.map[x] = []
			for(var y=0; y<height; y++){
				var Tile = null

				if(Math.random() < 0.5)
					Tile = gameData.makeEntity(gameData.entities.world.tile.ground)
				else
					Tile = gameData.makeEntity(gameData.entities.world.tile.wall)

				this.map[x][y] = Tile
				log.debug(Tile)
			}
		}
	},
	update: function(d){
		//render
	}
})
