var CES = require('ces')

var log = require('./log')
var Description = require('./component/default').Description


var index = 0
function Game(){
	this.id = index++
	log.info("Making world %s", this.id)
	this.world = new CES.World()

	//systems
	this.world.addSystem(new (require('./system/map'))(2, 2))
	//this.world.addSystem(new (require('./system/cleaner'))())

	log.info("World %s made", this.id)
}
Game.prototype.makePlayer = function(name){
	var player = new CES.Entity()
	player.addComponent(new Description(name, "Engineer"))
	//engineer components added
	////ISSUE. Entities are now objects. need to mimic their prorotypes now...
	///^ also their prorotypes are probably not using the right components....
	this.world.addEntity(player)
	log.info('World %s: player %s has entered the game', this.id, name)
	return player
}
Game.prototype.update = function(delta){
	this.world.update(delta)
}

module.exports = Game
