var log = require('./log')

var locals = {
	title: "Lonely Engineer Online",

	loc: {
		error: 'Oops. Something has broken.',
		errorMsg: "Code monkeys have been notified. Probably.",

		howToPlay: 'How to play',
		searchDefault: 'Everything',
		search: 'Search',
		play: 'Play',
		rankings: 'Rankings',
		you: 'You',
		login: 'Login',
		register: 'Register',

		indexTitle: 'What is this?',
		indexExcerpt1: '{title} is a realtime RTS and base defense type of game. '+
			'You are the only known person left. '+
			'There are monsters. Use your imagination to defeat them, or be overwhelmned.',
		indexExcerpt2: "You don't have to register, but nice to have all your eggs in one basket. "+
			"It is planned to track statistics "+
			"(engineer kills, monster kills, favourite weapon, wins, loses) "+
			"and give trophies on games, "+
			"and provide some sort of rewards "+
			"(ei, augments to your favourite weapons).",
		startGame: 'Start a game',

		enterName: 'You are about to enter the world. What do we call you?',
		characterName: 'Character name',
		playGame: 'Play {title}',

		guideExcept: 'TODO'
	},
	get: function(path){
		var bag = locals //traverse locals obj with path provided
		path = path.split('.')
		var next
		while(next=path.shift())
			bag = bag[next]

		if(path.length != 0) //couldn't find
			return undefined
		return bag
	},
	l: function (key){ //everything in here can reference app.locals
		var res = locals.get(key)
		if(res !== undefined){
			res = res.replace(/{.*?}/g, function(m){
				m = m.substring(1, m.length-1) //remove surrounding {}
				return locals.l(m)
			})
		}
		log.debug(key,'>>>',res)
		return res
	}
}


module.exports = locals