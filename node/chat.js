var io = require('socket.io')

var log = require('./log')

module.exports = function(io){
	io.on('connection', function(socket){
		//essentials
		socket.on('error', function(err){
			throw err
		})
		socket.on('disconnect', function(msg){
			//TODO doesn't seem to work
			var msg = 'Player ' + socket.name + ' has left the chat'
			socket.broadcast.emit('msg', msg, undefined, 4)
		})

		//chat
		socket.on('say', function(msg){
			socket.broadcast.emit('msg', msg, socket.name)
		})

		//player
		socket.on('name', function(name){
			socket.name = name
			var msg = socket.name + ' has entered the chat'
			socket.broadcast.emit('msg', msg, undefined, 4)
		})
		//start
		socket.emit('start')
		socket.emit('msg', 'Welcome to chat '+socket.name, undefined, 3)
	})
};