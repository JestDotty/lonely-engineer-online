/**
 * - has server time
 * - buffers all inputs
 * - has process inputs loop (45 ms)
 * - has its own game.core with last known absolute server state
 * - gettable state
 * - gettable last processes input
 */
var EventEmitter = require('events')
var util = require('util')

var log = require('../log')
var Core = require('../../game/core')

var Server = function(id, data){
	this.id = id
	this.players = []
	this.maxPlayers = 2
	this.game = new Core(id, data, log)
}
util.inherits(Server, EventEmitter)
Server.prototype.canConnect = function(){
	return this.players.length < this.maxPlayers
}
Server.prototype.join = function(name){
	var player = this.game.createPlayer(name)
	this.players.push(player)
	return player
}
Server.prototype.leave = function(player){
	var i = this.players.indexOf(player)
	this.players.splice(i, 1)
	Core.leave(player)
	if(this.players.length === 0)
		this.end()
}
Server.prototype.end = function(){
	//gather some stats or something
	//cleanup
	this.emit('end', Core.end)
}

module.exports = Server