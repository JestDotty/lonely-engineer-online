/**
 * - accepts user connections
 * - loads & serves game data to client & server
 * - has ref of all ongoing games
 * - buffers user inputs & forwards to game server
 *
 * - searches for open games
 * - makes new games (and passes time?)
 * - dissolves games
 */
var log = require('../log')
var data = require('./data')
var Server = require('./server')

function Lobby(){
	this.games = []
	this.recycledIds = []
}
Lobby.prototype.makeGame = function(){
	var id
	if(this.recycledIds.length === 0)
		var id = this.games.length
	else
		id = this.recycledIds.pop()
	var game = new Server(id, data)
	game.on('end', onGameEnd)
	this.games[id] = game
	return game
}
Lobby.prototype.onGameEnd = function(id, stats){
	//make sure all players are out
	var players = [] //get from io room
	players.forEach(function(s){
		this.exitGame(s)
	})

	//remove game
	this.games[id] = undefined
	recycledIds.push(id)

	//TODO do something interesting with gameplay stats
	log.info(stats)
}
Lobby.prototype.findGame = function(s){
	var game = this.games.find(function(g){
		return g.canConnect()
	});
	if(!game)
		game = makeGame()
	return game
}
Lobby.prototype.exitGame = function(s){
	if(s.player){ //make sure client is in a game/has player
		log.info(s.rooms)
		var id = s.rooms[0] //TODO is this right?
		this.games[id].leave(s.player)
		s.player = undefined
		s.leave(id)
		//TODO send something to the user about this game
		s.emit('end')
		if(game.players === 0)
			game.end()
	}
}

module.exports = function(io){
	var lobby = new Lobby()
	io.on('connection', function(s){
		s.on('error', function(err){
			throw err
		})
		s.on('disconnect', function(){
			lobby.exitGame(s)
		})
		s.on('name', function(name){
			s.name = name
		})

		s.inputs = [] //TODO have to think of this
		s.on('input', function(input){
			input.recieved = new Date()
			s.inputs.push(input)
		})
		s.on('exit', function(){
			lobby.exitGame(s)
		})

		s.emit('data', data, function(err){ //send game data on clients first connection
			s.on('join', function(){
				var game = findGame()
				s.join(game.id)
				s.player = game.join(s.name)
				s.emit('start', game.id)
			})
		});
	})
}