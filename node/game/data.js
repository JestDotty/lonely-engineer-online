var fs = require ('fs')
var path = require('path')
var util = require('util')

var CES = require('ces')
var find = require('find')

var log = require('../log')

var componentDir = path.join(__dirname, '..', '..', 'game', 'component')
var entityDir = path.join(__dirname, '..', '..', 'data')

var us = {
	components: {},
	entities: {},
	makeComponent: function makeComponent(componentName, args){
		var Component = us.components[componentName]
		if(!Component)
			return log.warn('component could not be found:', componentName)
		var component = Object.create(Component)

		if(!(args instanceof Array))
			args = [args]
		Component.apply(component, args)

		return component
	},
	makeEntity: function makeEntity(data){
		var entity = new CES.Entity()
		for(var component in data){
			if(!data[component])
				us.makeComponent(component, data[component])
		}
		return entity
	}
}

function loadJSON(filePath){ //loads JSON file, removes comments, & covnerts to javascript obj
	var data = fs.readFileSync(filePath).toString()
	data = data.replace(/\/{2,}[ \S]*$/gm, '') //remove comments
	return JSON.parse(data)
}
function ensureStructureAndAdd(obj, mount, adding){
	var next = mount.shift()
	if(!obj[next])
		obj[next] = {}
	if(mount.length == 0)
		obj[next] = adding
	else
		ensureStructureAndAdd(obj[next], mount, adding)
}

(function loadComponents(){
	var dataFolder = componentDir
	var files = fs.readdirSync(dataFolder)
	files.forEach(function(file){
		log.info('Loading', file)
		var cTypes = require(path.join(dataFolder, file))
		for(var component in cTypes){
			us.components[component.toLowerCase()] = cTypes[component]
			log.debug(file, ': loaded component:', component)
		}
	})
})();
(function loadEntities(){
	log.info('Loading entities')
	var dataFolder = entityDir

	function findEntities(mount, location){
		var files = fs.readdirSync(location)

		var defaultIndex = files.indexOf('default.json')
		var defaultData = {}
		if(defaultIndex >= 0){
			var location = dataFolder + path.sep + mount.join(path.sep)
			defaultData = loadJSON(path.join(location, files[defaultIndex]))
			files.splice(defaultIndex, 1)
		}

		files.forEach(function(file){
			var location = dataFolder + path.sep + mount.join(path.sep)
			var fullPath = path.join(location, file)
			var stats = fs.statSync(fullPath)

			if(stats.isDirectory()){ //we must go deeper
				var furtherMount = mount.slice()
				furtherMount.push(file)
				findEntities(furtherMount, fullPath)
			}else if(stats.isFile() && file.match('.json$')){ //omnomnomnom
				var name = path.basename(file, '.json')
				var data = loadJSON(fullPath)

				var entity = {}

				//make innards
				entity.name = name
					.replace('_', ' ') //underscores become spaces
					.replace(/\w\S*/g, function(txt){ //title case
						return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase()
					})
				for(var key in defaultData) //default, if any
					entity[key] = defaultData[key]
				for(var key in data) //loaded data
					entity[key] = data[key]

				var thisMount = mount.slice()
				thisMount.push(name)
				ensureStructureAndAdd(us.entities, thisMount, entity)
				log.debug(thisMount, ': loaded entity:', name)
			}
		})
	}
	findEntities([], dataFolder)
})();

log.trace(util.inspect(us, {showHidden: false, depth: null}))
module.exports = us