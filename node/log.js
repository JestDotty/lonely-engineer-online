var format = []
format.push("<{{file}} {{line}}:{{pos}} | {{method}}> {{message}}")
format.push({})
format[1].error = "<{{file}} {{line}}:{{pos}} | {{method}}> [{{timestamp}}] {{message}}\n{{stack}}"
format[1].warn = "<{{file}} {{line}}:{{pos}} | {{method}}> [{{timestamp}}] {{message}}"
format[1].info = "[{{timestamp}}] {{message}}"
//debug
format[1].trace = "<{{path}} {{line}}:{{pos}} | {{method}}>\n{{message}}"
//log

var logger = require('tracer').colorConsole({
	format: format,
	preprocess: function(data){ //for errors to display the right stack
		var input = data.args[0]
		if(input && input instanceof Error){
			data.args[0] = input.stack
		}
	},
	level:0
})
module.exports = logger