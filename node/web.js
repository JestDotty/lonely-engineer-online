var path = require('path')

var express = require('express')
var morgan = require('morgan')
var jade = require('jade')
var stylus = require('stylus')
var nib = require('nib')

var log = require('./log')

var app = express()
app.use(morgan('dev'))

app.set('views', path.join(__dirname, '..', 'jade'))
app.set('view engine', 'jade')

app.use(stylus.middleware({
	src: path.join(__dirname, '..', 'public'),
	compile: function compile(str, path){
		return stylus(str)
			.set('filename', path)
			.use(nib())
	}
  }
))

app.use(express.static(path.join(__dirname, '..', 'public')))
app.use('/favicon.ico', express.static(path.join(__dirname, '..', 'public', 'favicon.ico')))
app.use('/js/ces-browser.min.js', express.static(path.join(__dirname, '..', 'node_modules', 'ces', 'dist', 'ces-browser.min.js')))
app.use(function(req, res, next){
	res.set({
		'Cache-Control': 'no-cache'
	})
	next()
})

app.locals = require('./locals')
app.get('/', function(req, res){
  res.render('index')
})
app.get('/:page.jade', function(req, res){
	res.render(req.params.page)
})
app.use(function(err, req, res, next){
	log.error(err)
	res.status(500)
	res.render('error')
})
module.exports = app;