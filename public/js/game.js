localStorage.debug = ''

var controls = [
	{key: 37, control: 'left'},
	{key: 'a'.charCodeAt(0), control: 'left'},
	{key: 'A'.charCodeAt(0), control: 'left'},

	{key: 38, control: 'up'},
	{key: 'w'.charCodeAt(0), control: 'up'},
	{key: 'W'.charCodeAt(0), control: 'up'},


	{key: 39, control: 'right'},
	{key: 'd'.charCodeAt(0), control: 'right'},
	{key: 'D'.charCodeAt(0), control: 'right'},

	{key: 40, control: 'down'},
	{key: 's'.charCodeAt(0), control: 'down'},
	{key: 'S'.charCodeAt(0), control: 'down'},
]

$(window).load(function(){
	var params = window.location.search.replace('?', '')
	var name = params.match('name=(.*),?')
	if(name) //get matched element
		name = name[1]

	setupIO(name)
	setupSVG()
})

function loadSVGTile(canvas, url, cb){
	var tileSize = 32
	$.ajax({
		url: url,
		dataType: 'text',
		error: cb,
		success: function(data){
			data = data.replace(/<\/?svg.*?>/g, '') //remove outer svg tag used in svg files
			var g = canvas.group().svg(data) //wrap

			var bbox = g.bbox() //scale to game size
			g.scale(tileSize/bbox.width, tileSize/bbox.height, 0, 0)

			cb(undefined, g)
		}
	})
}

function setupSVG(){
	if(!SVG.supported){
		var browsersSupported = $('<a>', {href: 'http://caniuse.com/#feat=svg'})
		browsersSupported.append('Your browser does not support SVG. Please choose a browser that does.')
		$('#game').append(browsersSupported)
		return
	}
	var canvas = SVG('game').size(800, 600)
	//canvas.addClass('center-block')
	var bg = canvas.rect(0, 0)
	bg.attr({
		width: canvas.width(),
		height: canvas.height(),
		fill: '#555'
	})

	var engineer
	loadSVGTile(canvas, 'img/engineer.svg', function(err, svg){
		engineer = svg
	})
}

function writeToChat(msg, who, level){
	var li = $('<li>', {'class':'list-group-item'})
	if(level){
		switch(level){
			case 1: level = 'text-muted'; break;
			case 2: level = 'text-primary'; break;
			case 3: level = 'text-success'; break;
			case 4: level = 'text-info'; break;
			case 5: level = 'text-warning'; break;
			case 6: level = 'text-danger'; break;
		}
		li.addClass(level);
	}
	if(who){
		who = '<strong>'+who+':<strong> '
		li.append(who)
	}
	li.append(msg)
	$('#chat>ul').append(li);
	var chat = document.getElementById('chat');
	chat.scrollTop = chat.scrollHeight;
}

function setupIO(name){
	io = io()
	io.on('start', function(){
		io.emit('name', name)
		io.on('msg', writeToChat)
		$('#chat #say').click(function(){
			var input = $(this).parent().parent().find(':text')
			if(input.val()){
				io.emit('say', input.val())
				writeToChat(input.val(), name, 1);
				input.val('')
			}
		})

		//controls
		$('body').keydown(function(e){
			console.log('key down ' + e.which)
			controls.forEach(function(c){
				if(c.key === e.which)
					io.emit('activate', c.control)
			})
		})
		$('body').keyup(function(e){
			console.log('key up ' + e.which)
			controls.forEach(function(c){
				if(c.key === e.which)
					io.emit('deactivate', c.control)
			})
		})
	})
}
