/**
 * Uses game.core.js to render current game world
 */

function Render($game){
	this.$game = $game
	
}

function loadSVGTile(canvas, url, cb){
	var tileSize = 32
	$.ajax({
		url: url,
		dataType: 'text',
		error: cb,
		success: function(data){
			data = data.replace(/<\/?svg.*?>/g, '') //remove outer svg tag used in svg files
			var g = canvas.group().svg(data) //wrap

			var bbox = g.bbox() //scale to game size
			g.scale(tileSize/bbox.width, tileSize/bbox.height, 0, 0)

			cb(undefined, g)
		}
	})
}

function setupSVG(){
	if(!SVG.supported){
		var browsersSupported = $('<a>', {href: 'http://caniuse.com/#feat=svg'})
		browsersSupported.append('Your browser does not support SVG. Please choose a browser that does.')
		$('#game').append(browsersSupported)
		return
	}
	var canvas = SVG('game').size(800, 600)
	//canvas.addClass('center-block')
	var bg = canvas.rect(0, 0)
	bg.attr({
		width: canvas.width(),
		height: canvas.height(),
		fill: '#555'
	})

	var engineer
	loadSVGTile(canvas, 'img/engineer.svg', function(err, svg){
		engineer = svg
	})
}