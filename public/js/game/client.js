/**
 * Goes through lobby, sets up game core
 * Delegates rendering to render.js
 * Delegates input handling to control.js
 * Delegates synchronization to sync.js
 */

localStorage.debug = ''

function doLog(){
	return Array.prototype.slice.call(arguments).join(' ')
}

function Client(name, $game, $control){
	this.name = name
	this.control = new Control($control)
	this.render = new Render($game)

	this.log = {}
	this.log.log = doLog
	this.log.trace = doLog
	this.log.debug = doLog
	this.log.info = doLog
	this.log.warn = doLog
	this.log.error = doLog
}
Client.prototype.listen = function(){
	this.io = io.connect('/game')
	this.io.emit('name', this.name)
	//loading screen?
	this.io.on('data', function(data){
		//game
		this.data = data
		this.control.onProcessedInput = function(input){
			this.io.emit('input', input)
		}
		this.io.on('sync', function(state){
			this.sync.update(state)
		})

		//lobby
		this.io.on('start', function(id){
			this.game = new Game(id, this.data, this.log)
			this.sync = new Sync(this.control, this.game)
		})
		this.io.emit('join')
	})
}

$(window).load(function(){
	var params = window.location.search.replace('?', '')
	var name = params.match('name=(.*),?')
	if(name) //get matched element
		name = name[1]

	var client = new Client(name, $('#game'), $('body'))
	client.listen()
})