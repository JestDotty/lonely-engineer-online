/**
 * - Has user control scheme
 * - buffers user inputs & expends on server
 */

function Control($control){
	this.index = 0
	this.queue = []
	this.controls = [
		{key: 37, action: 'left'},
		{key: 'a'.charCodeAt(0), action: 'left'},
		{key: 'A'.charCodeAt(0), action: 'left'},

		{key: 38, action: 'up'},
		{key: 'w'.charCodeAt(0), action: 'up'},
		{key: 'W'.charCodeAt(0), action: 'up'},


		{key: 39, action: 'right'},
		{key: 'd'.charCodeAt(0), action: 'right'},
		{key: 'D'.charCodeAt(0), action: 'right'},

		{key: 40, action: 'down'},
		{key: 's'.charCodeAt(0), action: 'down'},
		{key: 'S'.charCodeAt(0), action: 'down'},
	]

	var that = this
	$control.keydown(function(e){
		processInput({
			key: e.which,
			state: true
		})
	})
	$control.keyup(function(e){
		processInput({
			key: e.which,
			state: false
		})
	})
}
Control.prototype.processInput = function(key){
	var that = this
	this.controls.forEach(function(c){
		if(c.key === key.key){
			var input = {
				i: that.index++,
				sent: new Date(),
				action: c.action,
				state: key.state
			}
			queue.push(input)
			that.onProcessedInput(input)
		}
	})
}
//will be overwritten
Control.prototype.onProcessedInput = function(data){
	console.log(data)
}