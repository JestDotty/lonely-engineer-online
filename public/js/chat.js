function Chat($chat){
	this.$chat = $chat
}
Chat.prototype.write = function(msg, who, level){
	var li = $('<li>', {'class':'list-group-item'})
	if(level){
		switch(level){
			case 1: level = 'text-muted'; break;
			case 2: level = 'text-primary'; break;
			case 3: level = 'text-success'; break;
			case 4: level = 'text-info'; break;
			case 5: level = 'text-warning'; break;
			case 6: level = 'text-danger'; break;
		}
		li.addClass(level);
	}
	if(who){
		who = '<strong>'+who+':<strong> '
		li.append(who)
	}
	li.append(msg)

	this.$chat.children('ul').append(li)
	this.$chat.scrollTop(this.$chat.height())
}
Chat.prototype.submit = function(){
	var input = this.$chat.find('input:text')
	if(input.val()){
		this.io.emit('say', input.val())
		this.write(input.val(), name, 1);
		input.val('')
	}
}
Chat.prototype.join = function(name){
	var that = this
	this.io = io('/chat')
	this.io.on('start', function(){
		that.io.emit('name', name)
		that.io.on('msg', this.write)
		that.$chat.children('#say').click(this.submit)
	})
}

$(window).load(function(){
	var params = window.location.search.replace('?', '')
	var name = params.match('name=(.*),?')
	if(name) //get matched element
		name = name[1]

	var chat = new Chat($('#chat'))
	chat.join(name)
})