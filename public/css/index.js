/**
 * - popover over picture of a thing to get summary of the thing
 * - carousal sooomething
 *
 *
 *
 * - tingodb
 * - need registration with email confirm. don't want spammers
 */

var express = require('express');
var morgan = require('morgan');

var jade = require('jade');
var stylus = require('stylus');
var nib = require('nib');

var log = require('./log');
var modulePackage = require('./package.json');
var port = 80;

var app = express()
app.use(morgan('dev'));
//for later: http://expressjs.com/resources/middleware.html

app.set('views', __dirname + '/views');
app.set('view engine', 'jade');
app.use(stylus.middleware({
	src: __dirname + '/public',
	compile: function compile(str, path){
		return stylus(str)
			.set('filename', path)
			.use(nib());
	}
  }
));
app.use(express.static(__dirname + '/public'));

//default state
app.locals = {
	//meta
	title: "StonersRate.it",
	description: modulePackage.description,
	author: modulePackage.author,

	//menu stuff
	categories: ['Movies', 'Games', 'TV Series'],

	//page stuff
	category: 'Movies',
	items: [{
		name: 'Once A Blue Moon',
		img: {
			thumb: null,
			full: null
		},

		stars: 3.7,
		leaves: 7.3
	}]
};

app.get('/', function(req, res){
  res.render('index');
});

app.get('/:category/:name')


app.listen(port);
log.info('listening at', port);