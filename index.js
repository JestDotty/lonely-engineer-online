/**
 * DESIGN:
 * - game page whites out chat when width of screen gets smaller
 *
 *
 * TODO:
 * - entity loading should have components easily accessible/copiable
 * - controls & movement component
 * - serving & drawing graphics
 */

var io = require('socket.io')
var http = require('http')
var util = require('util')

var express = require('express')
var morgan = require('morgan')
var jade = require('jade')
var stylus = require('stylus')
var nib = require('nib')

var log = require('./src/log')
var Game = require('./src/game')

var app = express()
app.use(morgan('dev'))
app.set('views', __dirname + '/views')
app.set('view engine', 'jade')
app.use(stylus.middleware({
	src: __dirname + '/public',
	compile: function compile(str, path){
		return stylus(str)
			.set('filename', path)
			.use(nib())
	}
  }
))
app.use(express.static(__dirname + '/public'))
app.use('favicon.ico', express.static(__dirname+'/public/favicon.ico')) //???
app.use(function(req, res, next){
	res.set({
		'Cache-Control': 'no-cache'
	})
	next()
})
app.locals = {
	title: "Lonely Engineer Online",

	loc: {
		error: 'Oops. Something has broken.',
		errorMsg: "Code monkeys have been notified. Probably.",

		indexTitle: 'What is this?',
		indexExcerpt1: '{title} is a realtime RTS and base defense type of game. '+
			'You are the only known person left. '+
			'There are monsters. Use your imagination to defeat them, or be overwhelmned.',
		indexExcerpt2: "You don't have to register, but nice to have all your eggs in one basket. "+
			"It is planned to track statistics "+
			"(engineer kills, monster kills, favourite weapon, wins, loses) "+
			"and give trophies on games, "+
			"and provide some sort of rewards "+
			"(ei, boosts to your favourite weapons).",
		startGame: 'Start a game',

		enterName: 'You are about to enter the world. What do we call you?',
		characterName: 'Character name',
		playGame: 'Play {title}'
	},
	get: function(path){
		var bag = app.locals //traverse locals obj with path provided
		path = path.split('.')
		var next
		while((next=path.shift()))
			bag = bag[next]

		if(path.length !== 0) //couldn't find
			return undefined
		return bag
	},
	l: function (key){ //everything in here can reference app.locals
		var res = app.locals.get(key)
		if(res !== undefined){
			res = res.replace(/{.*?}/g, function(m){
				m = m.substring(1, m.length-1) //remove surrounding {}
				return app.locals.l(m)
			})
		}
		log.debug(key,'>>>',res)
		return res
	}
}

var server = http.Server(app)
io = io(server)
var game = new Game()
var Dead = require('./src/component/states').Dead

io.on('connection', function(socket){
	//essentials
	socket.on('error', function(err){
		throw err
	})
	socket.on('disconnect', function(msg){
		//TODO doesn't seem to work
		if(socket.player)
			socket.player.addComponent(new Dead(null, 0, msg))
		socket.broadcast.emit('msg', 'Player ' + socket.name + ' has left the game', undefined, 4)
	})

	//chat
	socket.on('say', function(msg){
		socket.broadcast.emit('msg', msg, socket.name)
	})

	//player
	socket.on('name', function(name){
		socket.name = name
		socket.player = game.makePlayer(socket.name)
		socket.broadcast.emit('msg', 'Player ' + socket.name + ' has entered the game', undefined, 4)
	})
	socket.on('input', function(input){
		//input = forwards, move player forwards, etc
		//TODO
	})

	//start
	socket.emit('start')
	socket.emit('msg', 'Welcome to world '+game.id, undefined, 3)
})

app.get('/', function(req, res){
  res.render('index')
})
app.get('/:page.jade', function(req, res){
	res.render(req.params.page)
})

app.use(function(err, req, res, next){
	res.status(500).render('error')
	log.error(err)
})
var port = 81
server.listen(port)
log.info('Listening on port %s', port)
