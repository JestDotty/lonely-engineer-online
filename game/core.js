//both js and nodejs code start here
function Game(id, data, log){
	this.id = id
	this.d = data
	this.world = new d.CES.World()

	//systems
	this.world.addSystem(new (require('./system/map'))(d, log, 2, 2))
	this.world.addSystem(new (require('./system/cleaner'))(d, log))

	log.info('Made new game world', this.id)
}
Game.prototype.makePlayer = function(name){
	var player = new this.d.CES.Entity()
	player.addComponent(new this.d.Description(name, 'Engineer'))
	//engineer components added
	////ISSUE. Entities are now objects. need to mimic their prorotypes now...
	///^ also their prorotypes are probably not using the right components....
	this.world.addEntity(player)
	log.info('Player', name, 'has entered', this.id)
	return player
}
Game.prototype.update = function(delta){
	this.world.update(delta)
}
Game.prototype.leave = function(player){
	//TODO kill player
}
Game.prototype.end = function(cb){
	//??? do stats and cleanup
	cb(this.id, {})
}

module.exports = Game