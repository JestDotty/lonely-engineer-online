var CES = require('ces')

var stats = {

	Score: CES.Component.extend({
		name: 'score',
		kills: 0,
		//adds up all numbers of this fn & returns as final score
		getScore: function(){ //TODO put in a system?
			var score
			for(var k in this){
				if(!isNaN(this[k]))
					score += this[k]
			}
			return score
		}
	}),
	Health: CES.Component.extend({
		name: 'health',
		health: 10,
		init: function(health){
			this.health = health
		}
	}),
	Strength: CES.Component.extend({
		name: 'strength',
		strengths: [],
		init: function(){
			strengths.push(arguments)
		}
	}),
	Ineffective: CES.Component.extend({
		name: 'ineffective',
		ineffectives: [],
		init: function(){
			ineffectives.push(arguments)
		}
	}),
	Immunity: CES.Component.extend({
		name: 'immunity',
		immunities: [],
		init: function(){
			immunities.push(arguments)
		}
	}),

}


module.exports = stats