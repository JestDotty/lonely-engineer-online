var CES = require('ces')

var log = require('../../log')

var Score = require('../component/stats').Score

module.exports = CES.System.extend({
	update: function(d){
		//dead
		var entities = this.world.getEntities('dead')
		var desc
		var dead
		var score
		entities.forEach(function(e){0
			desc = e.getComponent('description')
			dead = e.getComponent('dead')
			if(dead.killer){
				score = dead.killer.getComponent('score')
				if(!score){
					score = new Score()
					dead.killer.addComponent(score)
				}
				score.kills += dead.score
			}

			log.info('%s was killed by %s for %s points. %s',
				desc.desc, dead.killer, dead.score, dead.msg)
		})

		//items?
		//etc
	}
})