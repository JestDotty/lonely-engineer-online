Lonely Engineer Online
---

You are the last person. Monsters spawn from Imagination. You mine Imagination to create automotons or structures. Find a way out because you are consumed.

#### Roadmap

~~0.1.0 : Website~~

~~0.2.0 : Setup canvas & chat~~

~~0.3.0 : Loading game data into ces~~

0.4.0 : Networking setup & moving engineer

0.5.0 : Map data

0.6.0 : Monsters & AI & player death & win condition

0.7.0 : Engineer builds things

0.8.0 : Multiple engineers per game

0.9.0 : Player controllable Monsters

1.0.0 : Polish & bug fixes (then publically host)

1.1.0 : User controllable lobby (see games, join friends)

1.2.0 : Cards of fate (selection will be in lobby)

1.3.0 : Engineer upgrades / skills

1.4.0+ : Fluff

Game design & brainstorming: https://trello.com/b/p4pRkxgP/lonely-engineer

Architecture
---

`jade` - jade that gets processed to webpages

`public` -  website files

`game` - shared game code (client & server) -- game logic

`data` - entity configuration & svg images

`node` - private node.js code -- serving jade, website architecture, lobby code, server processing, user stats

`public/game` - client side game code -- lobby, inputs, rendering, and prediction